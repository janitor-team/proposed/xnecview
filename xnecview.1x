.\" Copyright (c) 1999-2006, Pieter Tjerk de Boer (pa3fwm@amsat.org)
.\"
.\" This is free documentation; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as
.\" published by the Free Software Foundation; either version 2 of
.\" the License, or (at your option) any later version.
.\"
.\" The GNU General Public License's references to "object code"
.\" and "executables" are to be interpreted as the output of any
.\" document formatting or typesetting system, including
.\" intermediate and printed output.
.\"
.\" This manual is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public
.\" License along with this manual; if not, write to the Free
.\" Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139,
.\" USA.
.\"
.TH XNECVIEW 1
.SH NAME
xnecview \- A program for visualizing NEC2 input and output files
.SH SYNOPSIS
.B xnecview
.I "[options]"
.I "filename"
.I "[filename....]"

.SH DESCRIPTION
.B Xnecview
can visualize NEC2 input (structure of the antenna model) and output
data files
(gain as a function of direction, gain and impedance as a function of
frequency).
Structure and gain are shown as a three-dimensional picture which
can be rotated by the mouse.
The program will determine the type of data (input or output) from the files
themselves.

More information and some examples can be found on the web at
  http://www.pa3fwm.nl/software/xnecview/

.SH USAGE

On the commandline, the program expects to find one or more filenames,
each containing either NEC2 structure (input) data, or output data
(impedance and radiation pattern).
The program will determine the type of data from the data itself.
Depending on the data found in the file(s) specified, one or two windows
are opened.
Window 1 shows a 3D plot of the structure (wires etc.) of the antenna,
and/or the spatial distribution of the radiation.
Window 2 shows a set of graphs of several quantities (SWR, gain, etc.)
as a function of frequency.


.SS Window 1

This window shows the antenna's structure and/or the gain pattern.
Initially, the Z axis points up, the X axis points to lower left, and
the Y axis points to lower right (unless you changed those settings in
the source code).

The view can be manipulated using the mouse as follows:
.TP
.B rotate
move mouse while holding left button depressed
(additionally, keep CTRL depressed to drag only a partial picture for higher speed)
.TP
.B zoom
move mouse up/down while holding middle button depressed
(additionally, keep CTRL depressed to drag only a partial picture for higher speed);
alternatively, click left mouse button for zooming in
or click right mouse button for zooming out.
.TP
.B move picture
move mouse while holding right button depressed
(additionally, keep CTRL depressed to drag only a partial picture for higher speed)
.TP
.B go back to original view
click middle mouse button
.PP
The view can also be rotated using the arrow keys.
The keys PageUp and PageDown select a different frequency, if radiation
data is available at more than one frequency.

The top of the window contains a set of buttons and other indicators;
from left to right these are:
.TP
.B quit
to stop the program; keyboard shortcut: Q
.TP
.B reload
to reload the files; keyboard shortcuts: R and .
.PP
.TP
.B export
for saving the picture as an (encapsulated) PostScript or a PNG file.
.TP
.B none/struct/+tags/currents/animation
toggles displaying of antenna structure on and off;
in '+tags' mode, segment tag numbers are displayed too (which can be
helpful when trying to modify an antenna structure).
For display of currents and animations, see below.
.TP
.B none/slice/frame/opaque/near
toggles display of gain pattern: either none is shown,
or only slices in the coordinate planes, or the entire 3D structure
(of course subject to the availability of data in NEC's output file, and
thus ultimately to the RP cards used in the input file).
The 3D structure can either be shown as a wire mesh (i.e., transparent),
or as an opaque surface (i.e., with hidden lines removed); the latter
usually gives a clearer picture, but is often somewhat slower, and is
not available if the NEC output data do not cover a theta range from
0 to 90 or 180 degrees, and a phi range from 0 to between 270 and 360 degrees;
note that the gain surface is only opaque w.r.t. itself, not to other
elements of the picture such as the antenna structure.
For display of near fields, see under 'animation' below.
.TP
.B lin.P/lin.V/arrl/log
toggles gain scaling (linear in power, linear in voltage, ARRL-style,
or logarithmic with -40 dB at the center).
.TP
.B total/hor./vert./lhcp/rhcp/colour
determines the handling of radiation's polarization:
whether the gain shown is according to the total power regardless of
polarization, or only the horizontal/vertical/left-hand circular/right-hand circular
component.
Choosing "colour" also shows the total power, but uses colour to show
whether the radiation is mostly linearly polarized, or lhcp or rhcp.
This setting also influences the gain-vs-frequency plots in window 2,
and the currents display in window 1 (see below).
.TP
.B X, Y and Z
rotate view to viewing along X, Y or Z axis respectively.
A gain scale will appear, with
lines at several gain levels.
All of these gains are with respect to the maximum gain in the entire
set of output data.
.PP
Though xnecview should be quite liberal in accepting output data from
NEC, you might want to start out by using this line (card) in your input:
.nf
RP 0, 37, 72, 1000, 0, 0, 5, 5
.fi
This will instruct NEC to calculate the gain at 5 degree intervals.


.SS Window 2:

This window contains plots of several quantities as a function of
frequency, if the NEC output file contains data for several frequencies.
The following quantities can be plotted:
.TP
.B SWR
.TP
.B real and imaginary part of the input impedance
.TP
.B phase and magnitude of the input impedance
If the antenna has multiple sources, SWR and impedance are only plotted
for whichever source's data appears first in the output file.
.TP
.B maximum gain and corresponding front/back ratio
The gain as plotted is the maximum gain observed over the entire radiation
pattern; this may not be the direction in which the antenna was supposed
to radiate! The front/back ratio is just the ratio of the maximum observed
gain to the gain in the exactly opposite direction; again, this may not be 
the front/back ratio you're interested in, e.g. if the main lobe is elevated
so the 'back' direction points into the ground.

If a specific polarization (rather than total power) has been chosen
(by command-line option or by the button in the top row of window 1),
this also influences the graph.
Two gain lines then appear: a solid line showing the gain in the selected
polarization, and a dashed line showing the total gain (for comparison).
Also, two f/b lines appear: for both, the front power is only the selected
polarization component, while the back power is also the selected polarization
(solid line), or the total power (dashed line).
.TP
.B direction (phi and theta) of maximum gain
.TP
.B vgain and corresponding front/back ratio
This is the gain in the direction towards viewer
(as set by rotating the picture in window 1)
and the corresponding front/back ratio.
.PP

The row of buttons at the top have the following functions:
.TP
.B quit
to stop the program; keyboard shortcut: Q
.TP
.B reload
to reload the files; keyboard shortcuts: R and .
.TP
.B export
for saving the picture as an (encapsulated) PostScript or a PNG file.
.TP
.B Z0=...
for setting the reference impedance for SWR calculations;
furthermore, the impedance plots are limited to 20*Z0.
.TP
.B maxgain, vgain, SWR, Re/Im, phi/abs, and dir
for toggling the display of the graphs.
.PP

Finally, if radiation pattern data is available, a vertical line over the
entire height of the window shows the frequency at which the radiation
pattern is being shown in the other window. With a mouse click or drag,
or the keys PageUp, PageDown and arrow keys, another frequency can be
chosen.


.SS Display of current distribution:

Window 1 can also be used to display the distribution of the current
flowing in the antenna wires, if this information is available in the
NEC output file(s); by default, it is, but it may be switched off by
a 'PT' card in the NEC input.
This display is enabled by selecting 'currents' in the none/struct/+tags/currents
menu.
Then the thickness of each wire segment indicates the magnitude of the current
flowing there,
while the colour indicates its phase.
At the bottom of the window a few extra controls appear: two sliders for
changing the colours and scaling the thicknesses, and some buttons which
are discussed below.

Contrary to what might be expected, the magnitude and phase of the current
as plotted
are not necessarily directly the values present in the NEC output file.
Taking that data directly would typically not result in a meaningful
display, since there is a 180 degree phase ambiguity: if the endpoints
of a wire are exchanged, then the 'positive direction' in that wire is
reversed, so the phase calculated by NEC changes by 180 degrees even though
the antenna and its properties don't change.
Therefore, it is preferable to project the current in each segment onto some
reference direction, e.g., horizontal.
The result of this is a measure for the contribution of that segment
to the horizontally polarized radiation of the antenna.
The polarization actually used, is the one selected by the polarization
button in the top row; choosing "total" there (default), switches
the projection operation off, so 'raw' phases and magnitudes are used.
If left- or right-hand circular polarization is selected,
the projection is also not performed, but every current
gets an extra phase shift proportional to the angle its projection
perpendicular to the viewing direction makes with horizontal.

Actually, the phase displayed as discussed above is still not very interesting.
Consider the following:
if one segment is further away from the target to which the antenna is
supposed to radiate than another segment, then the radiation from the
former segment will incur a larger delay before reaching the target
than the radiation from the latter segment.
Effectively, this introduces another phase-shift, whose value depends
on the position of the segments in space.
Xnecview can compensate for this effect, by calculating this additional
phase-shift in the direction toward the viewer (i.e., perpendicular to
the screen); this option can be switched on and off by the first
button on the bottom row.

The second button locks the direction used in the phase-shift calculation;
its use can best be explained by an example.
Consider a yagi antenna which is aimed along the X axis.
Then in order to get the correct phase-shift, one needs to rotate
the picture such that the X axis points to the viewer.
Unfortunately, in that orientation all elements are behind each
other, so it is impossible to distinguish them in order to compare
their colours.
This problem is resolved by pressing the 'lock' button to lock the
phase-shift calculation and then
rotating the antenna to an orientation in which the elements are
distinguishable.


.SS Animated display of currents, charges and near fields:

Antennas as modeled by NEC are driven by a source (or more
than one) which applies a voltage or current to the antenna,
varying sinusoidally in time.
Consequently, the currents in the antenna wires, the charges
on the wires, and also the electric and magnetic field in the
surrounding space, vary sinusoidally in time too, at the
same frequency as the driving force, but possibly with a
different phase.
The display of the currents as described in the previous section
represents these time-varying currents by their amplitude
(thickness in the picture) and phase w.r.t. the source (colour
in the picture).

For some purposes, this is not very intuitive.
Therefore, xnecview can also show the currents (and charges and
field strengths) exactly as they vary in time: an animation.
Basically, the process which in reality happens at a frequency
of thousands or more cycles per second is slowed down to a
frequency of about 1 cycle per second, and at that speed
the currents and charges are displayed.

The animated display of currents and charges is enabled by
selecting 'animation' from the none/struct/+tags/currents/animation
menu.
Then each segment of each wire is replaced by a short blue line, one
end of which is at the center of the wire, while the other
end indicates the direction and (relative) magnitude of the
current.
Furthermore, around each segment a square is drawn. This
square represents the charge built up on that segment.
The size of the square is proportional to the magnitude of the
charge, while the colour shows the sign: cyan for positive
charge, magenta for negative.

The animated display of the electric and magnetic field near
the antenna is chosen by selecting 'near' from the none/slice/frame/near
menu.
Then at every point for which near field data is found in the
NEC output file, three coloured lines (vectors) are drawn.
A red one indicates the direction and (relative) magnitude of
the electric field,
and a green one indicates the direction and (relative) magnitude of
the magnetic field.
From the electric and magnetic field vectors, the so-called Poynting
vector is calculated, and displayed in yellow. This vector can be
interpreted as the flow of energy; see a textbook on electromagnetic
theory for details.

When either or both of the animated displays is selected, an
additional set of controls appears at the bottom of the window.
The left four of these are sliders to control the scaling of
(from left to right) currents, charges, electric and magnetic
field strength.
To the right of these, an on/off control labelled 'P' is shown,
which controls whether or not the Poynting vectors are drawn.
The rightmost slider controls the speed of the animation: if
your computer is fast enough, the number at the slider is the
number of animated cycles per second.
By setting this slider to 0, or hitting the 'z' key,
the animation can be frozen.
Then the phase can be changed back and forth by typing '<' and '>'
on the keyboard.

Obviously, xnecview can only show currents, charges and
near fields if such information is available in the NEC
output file being visualized.
As discussed earlier in this manual, the inclusion of
currents is controlled by the PT card in the NEC input.
The inclusion of charge information is controlled by the
PQ card, and the calculation of near electric and magnetic
fields is controlled by NE and NH cards, respectively.
Examples are:
.nf
PQ  0,  0
NE  0,  1,20,20,  0,0.05,0.05,  0,0.05,0.05
NH  0,  1,20,20,  0,0.05,0.05,  0,0.05,0.05
.fi
These instruct NEC to include the charge information,
and to calculate the near fields at 20 x 20 points in a
grid with stepsize 0.05, in the Y-Z-plane.
For more information see NEC documentation.

.SH COMMAND-LINE OPTIONS

In normal usage of xnecview, command-line options (other than the
names of the files to be displayed) are rarely needed.
However, they can be useful to bring xnecview quickly in
the desired state, or to use xnecview for non-interactive,
automated generation of plots.

Command-line options can not only be given on the command line
with which xnecview is started, but they can also be embedded
as a CM card (line) in the NEC input file to be read.
In order for the content of a CM card to be recognized as
xnecview options, the CM card should contain the word
xnecview: (including the colon) before those options.

The following options are available:
.TP
.B -h, --help
show usage information
.TP
.B --struct     
set structure view to 'struct'
.TP
.B --tags       
set structure view to 'struct+tags'
.TP
.B --currents   
set structure view to 'currents'
.TP
.B --animation  
set structure view to 'animation'
.TP
.B --slice      
set radiation view to 'slice'
.TP
.B --frame      
set radiation view to 'frame'
.TP
.B --opaque      
set radiation view to 'opaque'
.TP
.B --near       
set radiation view to 'near field'
.TP
.B --linpower   
set radiation scale linear in power
.TP
.B --linvoltage 
set radiation scale linear in voltage
.TP
.B --arrl       
set radiation scale to ARRL style
.TP
.B --log        
set radiation scale to logarithmic
.TP
.B --pol=x
choose polarization; x may be total, hor, vert, lhcp, rhcp or colour .
.TP
.B --qscale num 
set charges scale (animation)
.TP
.B --iscale num 
set currents scale (animation)
.TP
.B --escale num 
set electric field scale
.TP
.B --hscale num 
set magnetic field scale
.TP
.B --hidepoynting 
hide Poynting vector in near field display
.TP
.B --afreq num  
set animation frequency (Hz)
.TP
.B --aphase num 
set animation phase (degrees)
.TP
.B --aupdate num 
set animation update interval (milliseconds). Default is 100,
but on a slow computer and/or with a large data set
it may be useful to set the update interval higher.
Conversely, on a fast computer and with a simple data set,
a smaller setting provides smoother movement.
.TP
.B --freq num   
set frequency (MHz)
.TP
.B --z0 num
set reference impedance (ohm)
.TP
.B --expeps filename 
no X11 display, just export picture to .eps-file
.TP
.B --exppng
no X11 display, just export picture to .png-file
(only available if linked against the libpng library)
.TP
.B --view phi,theta,zoom,trx,try 
set viewing direction and zoom
.PP
Note: typing 'v' in window 1 writes the current values for
all of these settings to the standard output.



.SH AUTHOR
Pieter-Tjerk de Boer; Internet e-mail: pa3fwm@amsat.org,
amateur packet-radio: PA3FWM @ PI8DAZ.#TWE.NLD.EU.

