/*  XNECVIEW - a program for visualizing NEC2 input and output data
 *
 *  Copyright (C) 1998-2002, Pieter-Tjerk de Boer -- pa3fwm@amsat.org
 *
 *  Distributed on the conditions of version 2 of the GPL: see the files
 *  README and COPYING, which accompany this source file.
 */

/* ------------------------------------------------------------------------ */

/* some defines: */

#define ini_phi 30         /* initial rotation around Z-axis */
#define ini_theta 70       /* initial tilting of Z-axis */
#define ini_zoom 1.0       /* initial zoom-factor */
#define ini_trx 0          /* initial horizontal displacement of origin w.r.t. center of window */
#define ini_try 0          /* initial vertical displacement of origin w.r.t. center of window */

#define ini_winsize 500    /* initial size of window (pixels) */

#define MAXWIRES 2000      /* maximum number of wires */
#define MAXSURFACES 1000   /* maximum number of surfaces */
#define MAXEXCIS 100       /* maximum number of excitations */
#define MAXLOADS 1000      /* maximum number of loads */
#define MAXNETWS 30        /* maximum number of "networks" (including transmission lines) */

#define MAXFREQS 100       /* maximum number of frequencies in the NEC output file */
#define MAXSEGMENTS 600    /* maximum number of segments in the currents data in the NEC output file */
#define MAXPATCHES 600     /* maximum number of surface patches in the NEC output file */

#define Axislen 1.1        /* length of axes, compared to largest dimension of the antenna or gain grid */
#define GAINSIZE 3.0       /* ratio between size of gain grid and largest dimension of the antenna */

#define C_BG   "white"     /* color of the background */
#define C_AXIS "black"     /* color of the axes */
#define C_WIRE "blue"      /* color of the wires */
#define C_SURF "green3"    /* color of the front of surfaces */
#define C_BACK "cyan"      /* color of the back of surfaces */
#define C_GAIN "red"       /* color of the gain pattern */
#define C_SCALE "gray70"   /* color of the gain scale */
#define C_EXCI "orange"    /* color of wire segment containing excitation */
#define C_LOAD "brown"     /* color of loaded wire segment */
#define C_NETW "grey"      /* color of networks and transmission lines */
#define C_INACTIVE "gray86"   /* color in currents display of segments in which negligibly little current flows */
#define C_EFIELD "red"        /* color of electric field vectors */
#define C_HFIELD "green2"     /* color of magnetic field vectors */
#define C_POYNTING "yellow2"  /* color of Poynting vectors */
#define C_QPOS "cyan3"        /* color of positive charge */
#define C_QNEG "magenta"      /* color of negative charge */
#define C_GAIN_LIN "red"            /* color of gain when mostly linearly polarized (i.e., axial ratio < Polthr ) */
#define C_GAIN_LHCP "violet red"    /* color of gain when mostly left-hand circularly polarized */
#define C_GAIN_RHCP "orange"        /* color of gain when mostly right-hand circularly polarized */

#define NC_phase 64        /* number of different hues for the phase scale; must be a multiple of 4 */
#define Default_anim_update_interval 100   /* time (in milliseconds) between updates of the animation display */

#define XFONT "6x10"       /* font for text in the on-screen drawing */
#define PSFONT "helvetica" /* font for postscript output (size is derived by scaling the X font) */

#define R0  50.0           /* default reference impedance for SWR calculation */

#define Polthr (M_SQRT2-1)  /* threshold of axial ratio used in polarization-colouring */
